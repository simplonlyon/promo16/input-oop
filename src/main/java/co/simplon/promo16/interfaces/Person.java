package co.simplon.promo16.interfaces;

/**
 * Ici la classe Person implémente l'interface IExample, elle pourra donc être utilisée comme argument
 * d'une méthode attendant un IExample comme type de donnée ou à l'intérieur d'une liste de IExample etc.
 */
public class Person implements IExample, IHouseItem {

    public String greeting(String name) {
        
        return "Hello, "+name+", I am human";
    }

    public String tellName() {
        return "i am jean";
    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void use() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String draw() {
        return "🧍";
    }

}
