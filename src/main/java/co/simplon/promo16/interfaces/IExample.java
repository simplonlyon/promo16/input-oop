package co.simplon.promo16.interfaces;

/**
 * Une interface va permettre de définir un "contrat" que devront remplir les classes qui l'implémenteront
 * On dit à une classe "tu peux être typée en tant que IExample, à condition que tu implémentes toutes les
 * méthodes que je définis dans cette interface".
 * L'intérêt sera de pouvoir regrouper ou juste utiliser d'une manière similaires des objets n'ayant
 * potentiellement aucun point commun, à part le fait qu'ils implémentent une même interface.
 * Exemple de la vie réelle : Un interupteur, qui peut être soit allumé, soit éteint. Qu'est-ce qui
 * se passe quand on allume l'interupteur ? Et bien ça dépend de qui implémente l'interupteur, si c'est
 * une ampoule, ça va allumer la lumière, si c'est un blender ça fait mixer des trucs, si c'est 
 * une vieille horloge, ça va faire sonner le carillon. Ces trois objet ampoule/blender/vieille horloge ont
 * pas grand rapport les un avec les autres, mais s'ils implémentent tous les 3 l'interface "interupteur",
 * alors je sais que je peux tous les trois les utiliser en "allumant" ou en "éteignant" l'interupteur
 */
public interface IExample {
    /**
     * Une interface ne peut contenir que des signature de méthode public, c'est à dire son type de retour,
     * son nom et ses arguments et leur types, et c'est tout. L'interface n'est pas faite pour contenir
     * le code (l'implémentation) de comment marche la méthode en question, ça ce sera le rôle des classes
     * concrètes qui implémenteront cette interface.
     * Ici on définit que les classes qui implémenteront IExample devront avoir une méthode greeting qui
     * renvoie du String et attend du String en paramètre
     * @param name Le nom de la personne à qui dire bonjour
     * @return La phrase de bonjour
     */
    public String greeting(String name);

}
