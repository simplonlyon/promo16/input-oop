package co.simplon.promo16.interfaces;

import java.util.ArrayList;
import java.util.List;

public class SmartHouse {
    List<IHouseItem> items = new ArrayList<>();
    IHouseItem[][] grid = new IHouseItem[10][10];


    

    /**
     * Méthode qui permet d'ajouter un nouvel objet dans la maison à une position
     * donnée
     * @param item L'objet à rajouter dans la maison, un objet ne peut pas être 2 fois dans la même maison
     * @param x coordonnée x de placement de l'item
     * @param y coordonnée y de placement de l'item
     */
    public void placeItem(IHouseItem item, int x, int y) {
        if(!items.contains(item) && grid[x][y] == null) {
            grid[x][y] = item;
            items.add(item);
        }
    }
    /**
     * Afficher la maison dans son état actuel avec ses items positionnés dedans
     */
    public void displayHouse() {
        /**
         * Une première boucle qui va parcourir chaque ligne contenu dans la grid,
         * et dedans une deuxième boucle qui parcour chaque élément de chaque ligne
         */
        for (IHouseItem[] row : grid) {
            for (IHouseItem item : row) {
                System.out.print(" ");

                if(item == null) {
                    System.out.print(". ");
                } else {
                    System.out.print(item.draw());

                }
                System.out.print(" ");
            }
            System.out.println();

        }
    }
    
}
