package co.simplon.promo16.interfaces;

import java.util.ArrayList;
import java.util.List;

public class Car implements ITransport{
    private List<Person> passengers = new ArrayList<>();
    private int seats;
     

    public Car(int seats) {
        this.seats = seats;
    }

    public void startEngine() {

    }

    public boolean fillFuel(int value) {

        return false;
    }


    @Override
    public boolean moveToDestination(String address) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean hasRoom() {
        return seats > passengers.size();
    }

    @Override
    public void addPassenger(Person person) {
        
        
        if(hasRoom()) {
            passengers.add(person);
        }
        
    }
    
}
