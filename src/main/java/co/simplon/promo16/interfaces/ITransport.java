package co.simplon.promo16.interfaces;

public interface ITransport {
    boolean moveToDestination(String address);
    boolean hasRoom();
    void addPassenger(Person person);
}
