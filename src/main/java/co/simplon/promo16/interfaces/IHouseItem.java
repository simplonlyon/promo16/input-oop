package co.simplon.promo16.interfaces;

public interface IHouseItem {
    /**
     * Méthode permettant de savoir si l'objet est déplaçable ou non
     * @return True si déplaçable, false si non
     */
    boolean canMove();
    /**
     * Méthode permettant de lancer la fonctionnalité principale de l'item
     */
    void use();
    /**
     * Méthode permettant de récupérer une représentation visuelle de l'item
     * @return La string représentant l'item
     */
    String draw();
}
