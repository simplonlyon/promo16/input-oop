package co.simplon.promo16.todo;

import java.util.Scanner;

public class ScannerTodoList {
    private TodoList todoList;
    private boolean over = false;

    public ScannerTodoList() {
        this.todoList = new TodoList();
    }


    public void start() {
        Scanner scanner = new Scanner(System.in);

        while(!over) {
            System.out.println("What to do ?");
            String input = scanner.nextLine();
            todoList.addTask(input);
            System.out.println(todoList.display());
        }

        scanner.close();
    }
}
