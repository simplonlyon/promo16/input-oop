package co.simplon.promo16.todo;

import java.util.ArrayList;
import java.util.List;

public class TodoList {
    private List<Task> taskList = new ArrayList<>();

    public List<Task> getList() {
        return List.copyOf(taskList);
    }

    public void addTask(String label) {
        Task task = new Task(label);
        taskList.add(task);
    }

    public void actionTask(int index) {
        taskList.get(index).toggleDone();
    }

    public void clearDone() {
        // taskList.removeIf(item -> item.isDone());
        
        for (int i = 0; i < taskList.size(); i++) {
            if(taskList.get(i).isDone()) {
                taskList.remove(i);
                i--;
            }
        }
        
        /*
        for (int i = taskList.size()-1; i >= 0; i--) {
            if(taskList.get(i).isDone()) {
                taskList.remove(i);
            }
        }
        */

        // Iterator<Task> iterator = taskList.iterator();
        // while(iterator.hasNext()) {
        //     Task task = iterator.next();
        //     if(task.isDone()) {
        //         iterator.remove();
        //     }
        // }
        
    }
    /**
     * Méthode qui génère une chaîne de caractère d'affichage
     * de la TodoList actuelle
     * @return La chaîne de caractère représentant la TodoList
     */
    public String display() {
        String todoListString = "";
        for (Task task : taskList) {
            todoListString += task.display() +"\n";
        }
        return todoListString;
    }


}
