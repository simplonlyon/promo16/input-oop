package co.simplon.promo16.todo;

import java.util.UUID;

public class Task {
    private String uuid;
    private String label;
    private boolean done;

    public Task(String label) {
        this.label = label;
        this.done = false;
        this.uuid = UUID.randomUUID().toString();
    }

    public void toggleDone() {
        done = !done;
    }

    public String getUuid() {
        return uuid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }

    public String display() {
        if(!done) {
            return "☐ "+label;
        }
        return "☒ "+label;
    }
    
}
