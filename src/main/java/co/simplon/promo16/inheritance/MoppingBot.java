package co.simplon.promo16.inheritance;

/**
 * On a ici un Robot serpillière qui est une spécialisation du CleaningBot, afin de montrer
 * qu'on peut tout à fait hériter d'une classe qui hérite d'une autre.
 * Le MoppingBot est donc une sorte de CleaningBot, mais c'est aussi une sorte de Robot, il possède donc
 * toutes les méthodes et propriétés de ces deux classes en plus des siennes
 */
public class MoppingBot extends CleaningBot {
    private boolean dryClean = true;
    private int waterLevel = 5;

    //Ici le constructeur est commenté pour montrer que ça marche quand même, puisqu'on a un constructeur vide sur le parent
    // public MoppingBot() {
    //     super();
    //    
    // }

    /**
     * Méthode qui passe le robot du mode aspirateur au mode serpillère
     * @return Renvoie true s'il est en mode aspirateur et false si en mode serpillère
     */
    public boolean toggleDryCleaning() {
        
        return dryClean = !dryClean;
    }

    /**
     * Ici, on fait ce qu'on appel une "surcharge" ou une "redéfinition" de la méthode clean() du parent.
     * C'est à dire qu'on crée une méthode avec la même signature que dans le parent (même type de retour
     * même nom et même arguments). Si on appel la méthode clean sur une instance de MoppingBot, cette méthode
     * sera appelée plutôt que celle du parent
     */
    @Override
    public void clean() {
        if(!dryClean && waterLevel >= 5)  {
            waterLevel -= 5;
            discharge(15);
            System.out.println("Mopping the floor");
        } else {
            //Ici, on fait en sorte d'appeler la méthode clean du parent si jamais on est en mode aspirateur.
            //Puisqu'on a redéfinie la méthode clean, la seule manière d'accéder à l'originale est en passant
            //par le mot clef super qui représente le parent
            super.clean();

        }
    }
    
}
