package co.simplon.promo16.inheritance;

import co.simplon.promo16.interfaces.IExample;

public class ChargingStation implements IExample {
    private int power = 22;
    /**
     * Méthode qui va permettre de charger des Robot. Puisque le type de l'argument est Robot, n'importe
     * quelle instance qui est un Robot ou hérite de près ou de loin d'un Robot pourra être acceptée, elle
     * n'aura juste que les méthode du Robot le temps de cette méthode.
     * @param robot Un Robot ou une classe Enfant/Petit Enfant/etc. de Robot
     */
    public void connect(Robot robot) {
        
        robot.charge(power);
    }

    @Override
    public String greeting(String name) {
        
        return "boup boup boup";
    }
}
