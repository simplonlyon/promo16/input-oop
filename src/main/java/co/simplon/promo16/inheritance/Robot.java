package co.simplon.promo16.inheritance;

import java.util.UUID;

import co.simplon.promo16.interfaces.IExample;
import co.simplon.promo16.interfaces.IHouseItem;
/**
 * Robot est une classe qui représente un robot de base qui sera ensuite héritée pour créer
 * des robots plus spécialisés.
 * Les méthodes et propriétés contenues dans cette classe permettra de créer une base de comportements
 * communs pour tous les futurs Robot qui hériteront de celle ci
 * 
 * Autre sujet : Cette classe Robot implémente l'interface IExample, cela signifie qu'une instance de 
 * Robot, ou de n'importe lequel de ses enfants, pourra être utilisé EN TANT que IExample (par exemple
 * si on a une liste de IExample, ou une méthode qui attend un IExample en argument ou autre)
 * Pour implémenter l'interface, il a fallut que Robot "implètemente" également toutes les méthodes
 * définies à l'intérieur, c'est à dire "coder les méthodes en question pour de vrai"
 * 
 */
public class Robot implements IExample, IHouseItem{
    private int battery = 100;
    private String serialNumber;

    /**
     * Un constructeur permettant d'indiquer un niveau de battery à l'initialisation, utile notamment
     * pour les tests
     * @param battery Le niveau de battery à initialisé
     */
    public Robot(int battery) {
        this.battery = battery;
    }

    /**
     * Le constructeur par défaut (qui sera d'ailleurs appelé par défaut par toutes les classes enfants
     * de Robot), qui ici ne fait qu'initialiser le serialNumber
     */
    public Robot() {
        this.serialNumber = UUID.randomUUID().toString();
    }
    
    /**
     * Une méthode permettant d'augmenter la valeur de la battery en controlant que celle ci ne dépasse
     * pas les 100
     * @param value combien de battery on souhaite ajouter
     * @return le niveau de battery suite à l'augmentation
     */
    public int charge(int value) {
        battery += value;
        if(battery > 100) {
            battery = 100;
        }
        return battery;
    }
    /**
     * Même méthode que le charge, mais pour baisser le niveau de battery sans aller en dessous de zéro
     * A noter que cette méthode est protected plutôt que public, on fait ça car on peut considérer que
     * ce qui charge un Robot c'est quelque chose d'éxtérieur (une prise, un panneau solaire, etc.), alors
     * que le fait de décharger ne sera déclencher que par les Robot lors de leurs diverses action et n'aura
     * pas vocation à être appelée depuis l'extérieur
     * @param value De combien le robot se décharge
     * @return La valeur de la battey après décharge
     */
    protected int discharge(int value) {
        battery -= value;
        if(battery < 0) {
            battery = 0;
        }
        return battery;
    }
    /**
     * Une méthode qui permet de savoir si le Robot est en capacité de faire des actions, vis à vis de 
     * son niveau de battery
     * @return true s'il reste de la battery, false sinon
     */
    public boolean isOn() {
        
        return battery > 0;
    }

    /**
     * Ici, on a l'implémentation de la méthode de l'interface IExemple, il faut forcément respecter 
     * la signature exact de la méthode pour que l'implémentation soit valide
     */
    @Override
    public String greeting(String name) {
        
        return "Hello Human "+name+" I am ROBOT N°"+serialNumber;
    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void use() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String draw() {
        
        return "🤖";
    }


}
