package co.simplon.promo16.inheritance;

/**
 * La classe CleaningBot hérite de la classe Robot, cela signifie qu'il s'agit "d'une sorte de Robot",
 * elle possède donc toutes les propriétés et méthode de la classe Robot en plus de celles qu'on lui
 * définira.
 */
public class CleaningBot extends Robot {
    private int dustBag = 0;
    
    /**
     * Il n'est pas nécessaire de mettre un constructeur si la classe parent a déjà un constructeur "par défaut"
     * (un constructeur qui n'attend pas d'argument), donc ce constructeur est inutile, il est juste là pour
     * illustrer ce qu'il se passe de manière invisible quand on instancie une classe enfant :
     * Le constructeur de la classe enfant doit commencer par appeler le constructeur de la classe parent, avec le
     * mot clef super, avant de faire ses propres initialisation.
     */
    public CleaningBot() {
        super();
        
    }
    /**
     * La méthode clean va vérifier s'il y a assez de battery puis vérifier si le sac de l'aspirateur
     * est plein, et si c'est ok pour les deux, faire du ménage et décharger de la battery.
     * L'idée ici est d'illustrer le fait qu'une classe enfant peut avoir des méthodes qui lui sont propres
     * en plus de celles de sa classe parent. (ces méthodes supplémentaire ne seront accessible que si
     * la variable qui contient l'instance est du type CleaningBot, mais pas si la variable est de type Robot)
     */
    public void clean() {

        if(!isOn()){
            System.out.println("Robot out of power");
            return;
        }

        if( dustBag+5 < 100) {
            dustBag += 5;
            discharge(10);
            System.out.println("Dadidou, cleaning the house");
        }
    }

    @Override
    public String draw() {
        return "🧹"+super.draw();
    }
}
