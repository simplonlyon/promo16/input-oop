package co.simplon.promo16;

import co.simplon.promo16.inheritance.CleaningBot;
import co.simplon.promo16.inheritance.Robot;
import co.simplon.promo16.interfaces.Person;
import co.simplon.promo16.interfaces.SmartHouse;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        // ScannerTodoList scannerTodo = new ScannerTodoList();
        // scannerTodo.start();

        // NumberGuesser guesser = new NumberGuesser();

        // guesser.play();

        // TodoList todoList = new TodoList();

        // todoList.addTask("Terminer l'exercice");
        // todoList.addTask("Ne pas Aller Manger");
        // todoList.addTask("Dormir jusqu'à 17h30");

        // todoList.actionTask(0);
        // todoList.actionTask(1);

        // System.out.println(todoList.display());

        // ChargingStation station = new ChargingStation();
        // CleaningBot cleaner = new CleaningBot();
        // cleaner.clean();

        // station.connect(cleaner);

        // MoppingBot mop = new MoppingBot();
        // mop.toggleDryCleaning();
        // mop.clean();
        // mop.clean();

        // List<IExample> list = new ArrayList<>();
        // list.add(new Person());
        // list.add(new MoppingBot());
        // list.add(new ChargingStation());

        // for (IExample iExample : list) {
        // System.out.println(iExample.greeting("Jean"));
        // }

        SmartHouse house = new SmartHouse();
        house.placeItem(new Robot(), 2, 4);
        house.placeItem(new CleaningBot(), 2, 3);
        house.placeItem(new Person(), 4, 4);

        house.displayHouse();
    }
}
