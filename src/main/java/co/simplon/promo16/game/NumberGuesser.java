package co.simplon.promo16.game;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * La classe qui représente le jeu consistant à deviner un nombre
 */
public class NumberGuesser {
    /**
     * Le nombre à deviner, initialisé à 5, on pourrait l'initialiser par une 
     * valeur random, ça serait mieux
     */
    private int answer = 5;
    /**
     * Est-ce que le jeu est fini
     */
    private boolean over = false;

    /**
     * Méthode qui vérifie si l'entrée donnée correpond ou non au nombre à deviner
     * @param input Le nombre qu'on veut vérifier si c'est lui
     * @return Un feedback qui varie selon si c'est plus grand, moins grand ou le bon nombre
     */
    public String guess(int input) {
        if (answer < input) {
            return "it's lesser than that";
        }
        if (answer > input) {
            return "it's greater than that";
        }
        over = true;
        return "Congratz";

    }
    /**
     * La méthode qui va faire la boucle à proprement parler, qui va demander
     * un input à l'utilisateur·ice à chaque tour de boucle et donner cet
     * input à la méthode guess
     */
    public void play() {
        Scanner scanner = new Scanner(System.in);
        while (!over) {
            //Ici, on fait un try-catch pour le cas où l'input ne serait pas un nombre
            try {

                int input = scanner.nextInt();
                String feedback = guess(input);
                System.out.println(feedback);
            } catch (InputMismatchException e) {
                System.out.println("Only numbers accepted");
                scanner.next();
            }
        }

        scanner.close();

    }
    /**
     * Un setter qui sera surtout utile pour les tests afin d'assigner une valeur 
     * à deviner directement dans le test 
     */
    public void setAnswer(int answer) {
        this.answer = answer;
    }
    /**
     * Un getter qui permet de savoir si le jeu est terminé ou pas
     */
    public boolean isOver() {
        return over;
    }
}
