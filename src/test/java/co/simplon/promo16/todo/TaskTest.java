package co.simplon.promo16.todo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TaskTest {
    @Test
    public void shoudDisplayUncheckedTask() {
        Task instance = new Task("Test");
        assertEquals("☐ Test", instance.display());
    }
    @Test
    public void shoudDisplayCheckedTask() {
        Task instance = new Task("Test");
        instance.toggleDone();
        assertTrue(instance.isDone());
        assertEquals("☒ Test", instance.display());
    }
}
