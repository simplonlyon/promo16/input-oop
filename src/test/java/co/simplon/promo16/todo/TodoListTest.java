package co.simplon.promo16.todo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TodoListTest {
    @Test
    public void testAddTask() {
        TodoList instance = new TodoList();
        
        assertEquals("", instance.display());

        instance.addTask("Test");
        
        assertEquals("☐ Test\n", instance.display());

    }
}
