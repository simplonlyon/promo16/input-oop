package co.simplon.promo16.inheritance;


import static org.mockito.Mockito.*;


import org.junit.Test;


public class ChargingStationTest {
    @Test
    public void testConnect() {
        ChargingStation instance = new ChargingStation();
        Robot toCharge = mock(Robot.class);

        instance.connect(toCharge);
        verify(toCharge).charge(22);
    }
}
