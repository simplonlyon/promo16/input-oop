package co.simplon.promo16.inheritance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldChargeAndReturnCurrentBattery() {
        Robot instance = new Robot(10);
        assertEquals(30, instance.charge(20));
        
    }
    @Test
    public void shouldChargeNotAbove100() {
        Robot instance = new Robot(100);

        assertEquals(100, instance.charge(20));
        

    }
}
