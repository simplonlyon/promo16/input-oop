package co.simplon.promo16.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class NumberGuesserTest {
    NumberGuesser guesser;

    @Before
    public void init(){
        guesser = new NumberGuesser();
        guesser.setAnswer(5);
    }

    @Test
    public void shouldSayGreater() {
        assertEquals("it's greater than that", guesser.guess(2));
        assertFalse(guesser.isOver());
    }
    @Test
    public void shouldSayLesser() {
        assertEquals("it's lesser than that", guesser.guess(6));
        assertFalse(guesser.isOver());

    }
    @Test
    public void shouldSayCongratz() {
        assertEquals("Congratz", guesser.guess(5));
        assertTrue(guesser.isOver());

    }
}
