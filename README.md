# input-oop
Un projet dans lequel on va voir les intéraction avec le user via la console ainsi que de la POO, genre héritage/interfaces/polymorphisme 


## Guess a Number
Utiliser le scanner pour faire deviner un nombre au user, si le nombre entrée est inférieur, on lui dit "it's greater than that", si le nombre est inférieur on lui dit "it's lesser than that" et si le bon, on dit bravo

Aides :
* Ça peut être une idée de définir une variable qui contiendra le nombre à deviner et de l'initialiser avec le nombre que vous souhaitez dedans
* On peut commencer par juste demander un seul truc avec un seul nextLine et faire les if derrière qui se baseront sur la réponse donnée pour afficher un message ou l'autre
* Une fois qu'on a fait ça, le but sera de faire que le programme nous redemande jusqu'à ce qu'on ait trouvé, pour ça il va falloir faire une boucle qui contiendra le nextLine, pourquoi pas un while, et faire une variable boolean qui indique si le jeu est fini ou pas (et en gros, le jeu est terminé lorsque la personne a trouvé)

### Rendre le code au moins partiellement testable
1. Créer une classe NumberGuesser, pourquoi pas avec une propriété contenant le nombre à deviner et une propriété contenant la valeur booléenne indiquant si le jeu est en cours ou non
2. Créer une méthode guess qui va attendre un int en entrée et renvoyé une string, et dans cette méthode mettre les if qu'on a fait précédemment mais en faisant un return de la phrase de réponse plutôt qu'un sysout
3. Tester la méthode guess
4. Créer une autre méthode run, qui va le Scanner et la boucle sur over, et qui à chaque tour de boucle, va appeler et sysout la méthode guess() 

## La TodoList
Faire une TodoList avec 2 classes, une Task et une TodoList et faire qu'on puisse ajouter des task à la liste, en supprimer et retirer toutes les tâches terminées
1. Créer une classe todo.Task qui ressemblera à ça : ![Task diagramme](diagrammes/task-uuid.png)
2. Faire une classe todo.TodoList qui sera composée d'une List de task ressemblant à ceci ![TodoList diagramme](diagrammes/todolist-java.png)
3. Créer un constructeur dans la Task qui attendra un label et initialisera le done à false et mettra un uuid à l'intérieur de la propriété du même nom. Pour ce faire, vous pouvez utilisez la méthode randomUUID() de la classe java.util.UUID
4. Dans la méthode addTask de la TodoList, utiliser le paramètre label pour créer une nouvelle instance de Task puis l'ajouter dans la liste de tasks
5. Dans la méthode clearDone(), faire une boucle sur toutes les tasks et retirer de la liste celles dont le done est à true
6. Créer une méthode actionTask(int index) dans TodoList qui va déclencher le toggleDone de la Task qui se trouve à l'index donné en paramètre

### Affichage de la TodoList
1. Créer dans la classe Task une méthode display() qui renverra une chaîne de caractère avec "☐ Label" dedans si la Task n'est pas done, et "☒ Label" si la Task est done
2. Dans la classe TodoList, créer une autre méthode display qui va :
   * Boucler sur chaque Task contenu dans la liste
   * A chaque tour de boucle, déclencher la méthode display de la Task
   * Rajouter un ptit saut de ligne à la fin de chaque tâche ("\n" le saut de ligne)
3. Rajouter 2-3 task dans la todoList via le main si c'est pas déjà fait, et afficher le résultat du display dans la console pour voir le résultat 

### Créer une User Interface avec un Scanner
1. Créer une classe ScannerTodoList qui aura en propriété une TodoList (initialisée dans le constructeur par exemple) et une méthode start()
2. Dans la méthode start, créer un scanner et faire une boucle comme on avait fait dans la méthode play du NumberGuesser
3. À l'intérieur de cette boucle, on va afficher avec un sysout "Enter a new task" puis faire un nextLine pour récupérer un input utilisateur·ice et utiliser cette input dans la méthode addTask de la TodoList. Afficher ensuite le résultat avec la méthode display 
4. Mettre le scanner en propriété de la la classe ScannerTodoList (pour pouvoir y accéder depuis d'autres méthodes), et créer une méthode addMenu, et dans cette méthode, on met le contenu du 3. dedans
5. A la place, dans le start, faire en sorte d'afficher les possibilités suivantes
```
1) Add a Task
2) Check task
3) Clear done
4) Exit
```
puis faire un nextInt() et si on récupère 1, alors on lance la méthode addMenu, si on récupère 4 alors on arrête la boucle et on ferme le scanner
6. Créer une méthode checkMenu qui va afficher l'état actuel de la todoList et demander "Which task do you want to check", et ensuite pareil, faire un nextInt() pour récupérer l'index de la task à cocher et déclencher un actionTask() avec. Rajouter un if dans le start qui déclenche le checkMenu
7. Et rajouter un dernier if dans le stat qui va déclencher le clearDone et afficher la todoList

(pas de correction à partir du 4. mais si on veut un truc complet, c'est un exemple de ce qu'il faudrait faire)

## Robot Inheritance
1. Créer une classe inheritance.Robot, qui aura comme propriété une battery initialisée à 100, un serialNumber en String qui sera initialisé en UUID
2. Créer une classe inheritance.ChargingStation qui aura comme propriété un power initialisé à 22
3. Dans la classe Robot, rajouter une méthode charge(int value) qui va augmenter la battery du nombre donné mais en faisant que ça dépasse pas 100
4. Dans la classe ChargingStation, rajouter une méthode connect(Robot robot) qui va déclencher la méthode charge du robot donné et charger sa battery de la valeur de power
5. Tester ces deux méthodes, ou au moins la méthode connect

### Faire hériter des robots
1. Créer une classe CleaningBot et faire que cette classe ait comme propriété un dustBag en int initialisé à 0 et qui aura comme méthode clean() qui ajoute 5 au dustBag si celui ci n'est pas à 100 et qui fait un sysout de "la li la lou I'm cleaning the house"
2. Faire que ce CleaningBot extends de la classe Robot, afin d'hériter de toutes ses propriétés et méthodes 
3. Créer dans le Robot une méthode protected int discharge(int value) qui va baisser la battery de la value, et la repasser à 0 si jamais elle tombe en dessous de zéro
4. Faire que la méthode clean du CleaningBot décharge la batterie de 5
5. Faire une méthode isOn qui renverra true si la battery est supérieure à 0 et false sinon, puis faire que le cleaningBot ne fasse clean que s'il est allumé

### Sélection et déplacement ([branche move-item](https://gitlab.com/simplonlyon/promo16/input-oop/-/tree/move-item))
1. Dans la SmartHouse créer une nouvelle propriété selected qui sera de type IHouseItem et qui sera null par défaut
2. Créer une méthode select() qui attendra 2 arguments int, un x et un y, qui vérifiera s'il y a quelque chose dans la grid à la position donnée, et si oui, qui le mettre à l'intérieur de la variable selected
3. Créer une méthode moveSelected qui attendra 2 arguments int x, y également (la destination), et qui vérifiera si on a bien quelque chose de sélectionné, si l'item sélectionné est déplaçable et si la destination est null
4. Si ces trois conditions sont vérifiées, alors on mettre l'élément sélectionné à sa destination 
5. Bug : Si on affiche la maison à nouveau, on constate que l'objet déplacé semble avoir été dupliqué, il faudrait effacer son ancienne position, mais du fait des contrainte client d'avoir une phase de sélection avant tout, ça complique la tâche. Essayer de trouver une manière de modifier le code pour faire qu'on puisse effacer l'ancien emplacement après déplacement
6. Pour corriger le bug, qui consiste à effacer la position précédente du selected, 3 possibilité :
   * Dans la méthode move, avant d'assigner le selected à sa nouvelle position, on fait une double boucle sur la grid et sur chaque cellule on vérifie si la valeur de la cellule correspond à selected, si oui, on la passe à null (et ensuite on lui assigne sa nouvelle position, comme on faisait déjà)
   * On rajoute 2 propriété xSelected et ySelected en Integer dans notre SmartHouse et au moment du select, on stock le x et y donné dans le select dans ces deux props, et on s'en ressert dans le moveSelected, en oubliant pas d'assigner à xSelected et ySelected la nouvelle position du selected une fois qu'on l'a déplacé
   * On crée une nouvelle classe SelectedItem qui aura 3 propriétés public, un IHouseItem, un int x et un int y, avec un constructeur pour ces 3 valeurs, et on lui crée une méthode updatePosition(int x, int y)  qui va venir mettre à jour sa position, et dans notre SmartHouse, au lieu d'avoir un IHouseItem selected, on aura un SelectedItem selected qui contiendra tout
7. Faire que les robot puissent se déplacer seulement s'ils ont de la battery (s'ils ont on)
8. Modifier le displayHouse pour faire en sorte de rajouter des [ ] autour de l'item actuellement sélectionné.

### Utiliser les item ([Branch use-item](https://gitlab.com/simplonlyon/promo16/input-oop/-/tree/use-item))
1. Dans la SmartHouse, rajouter une méthode useSelected qui juste déclenchera le use du IHouseItem actuellement sélectionné
2. Implémenter la méthode use() dans les différents IHouseItem, pour faire que...
   * La personne fasse un greeting avec "everybody" comme name
   * Le robot se décharge de 5
   * Le cleaningBot fasse un clean 

### Scanner pour faire une SmartHouse en ligne de commande ([Branch house-scanner](https://gitlab.com/simplonlyon/promo16/input-oop/-/tree/house-scanner))
1. Créer une classe HouseScanner qui contiendra une propriété SmartHouse, une propriété Scanner et une méthode start()
2. Faire un constructeur qui va attendre en argument une SmartHouse puis initialiser le Scanner
3. Dans la méthode start(), faire une boucle infini qui va afficher l'état actuel de la maison puis demander ce qu'on veut faire parmi : Select an Item, Add an Item et exit
4. Créer une méthode itemSelection qui attendra 2 coordonnées l'une après l'autre avec un nextInt et qui déclenchera un select sur la SmartHouse. Rajouter un if dans la start qui déclenchera la méthode itemSelection si on choisi "Select an Item"
5. Créer une méthode selectedMenu qui sera déclencher par le itemSelection, une fois un item sélectionné, et faire qu'elle liste les différentes actions possibles sur un item via une nouvelle boucle : Move, Use  ou Back (faire que après chaque action on déclenche un displayHouse() pour afficher l'état actuel de la maison)
6. Si on choisit Move, faire qu'on attende 2 coordonnées avec des scanner.nextInt() et les utiliser pour déplacer l'item en question
7. Si on choisit Use, juste déclencher le useSelected()
8. Si on choisit Back, ça stoppe la boucle actuelle (et donc nous renvoie sur la boucle principale) 